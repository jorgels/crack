# Simple password cracker

## Build and run
```bash
mkdir build
cd build
cmake ..
make

mpirun -n 4 ./crack -i ../data/shadow/sha512-2alnum -d ../data/dict/alnum.txt -l 2
```