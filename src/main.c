#include <crypt.h>
#include <mpi.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "util.h"

static bool run_master(int argc, char **argv, MPI_Datatype *, MPI_Datatype *);
static bool init_master(Options *options, Dictionary *dict, FILE **shadow_file, FILE **result_file, int argc, char **argv);
static void master_cleanup(FILE *shadow_file, FILE *result_file, Dictionary *dict);
static void master_crack(ExtendedCrackResult *result, Options *options, Dictionary *dict, ShadowEntry *entry, MPI_Datatype *, MPI_Datatype *);
static bool get_next_probe(ProbeConfig *config, Options *options, Dictionary *dict);
static void crack_job(CrackResult *result, CrackJob *job);
static void handle_result(Options *options, ExtendedCrackResult *result, OverviewCrackResult *overview_result, FILE *result_file);
static void handle_overview_result(Options *options, OverviewCrackResult *overview_result);

static bool run_worker(MPI_Datatype *crackjob_type, MPI_Datatype *result_type) {
    while (true) {
        CrackJob job;
        CrackResult result;
        MPI_Scatter(NULL, 0, NULL, &job, 1, *crackjob_type, 0, MPI_COMM_WORLD);
        if (job.action == ACTION_STOP) {
            return true;
        }
        if (job.action == ACTION_WAIT) {
            usleep(100);
            continue;
        }
        crack_job(&result, &job);
        MPI_Gather(&result, 1, *result_type, NULL, 0, NULL, 0, MPI_COMM_WORLD);
    }
}

int main(int argc, char **argv) {
    MPI_Init(&argc, &argv);
    int rank;
    bool success = false;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Datatype crack_job_type, temp_type;
    int blocklen[5] = {
        MAX_SHADOW_PASSFIELD_LENGTH + 1,
        1,
        1,
        MAX_PASSWORD_LENGTH + 1,
        1};
    MPI_Datatype types[5] = {MPI_CHAR, MPI_INT, MPI_INT, MPI_CHAR, MPI_INT};
    MPI_Aint disp[5] = {
        offsetof(CrackJob, passfield),
        offsetof(CrackJob, alg),
        offsetof(CrackJob, salt_end),
        offsetof(CrackJob, probe),
        offsetof(CrackJob, action)};
    MPI_Type_create_struct(5, blocklen, disp, types, &temp_type);
    MPI_Type_create_resized(temp_type, 0, sizeof(CrackJob), &crack_job_type);
    MPI_Type_commit(&crack_job_type);

    MPI_Datatype result_temp_type, result_type;
    int bl[2] = {1, MAX_PASSWORD_LENGTH + 1};
    MPI_Datatype result_types[2] = {MPI_INT, MPI_CHAR};
    MPI_Aint displacement[2] = {offsetof(CrackResult, status), offsetof(CrackResult, password)};
    MPI_Type_create_struct(2, bl, displacement, result_types, &result_temp_type);
    MPI_Type_create_resized(result_temp_type, 0, sizeof(CrackResult), &result_type);
    MPI_Type_commit(&result_type);

    if (rank == 0) {
        success = run_master(argc, argv, &crack_job_type, &result_type);
    } else {
        success = run_worker(&crack_job_type, &result_type);
    }

    MPI_Finalize();
    return success ? EXIT_SUCCESS : EXIT_FAILURE;
}

static bool run_master(int argc, char **argv, MPI_Datatype *type, MPI_Datatype *result_type) {
    Options options = {};
    Dictionary dict = {};
    FILE *shadow_file = NULL;
    FILE *result_file = NULL;

    bool init_success = init_master(&options, &dict, &shadow_file, &result_file, argc, argv);

    // If init successful, try to crack all shadow entries
    if (!options.quiet) {
        printf("\nEntries:\n");
    }
    OverviewCrackResult overview_result = {};
    if (init_success) {
        ShadowEntry shadow_entry;
        while (get_next_shadow_entry(&shadow_entry, shadow_file)) {
            ExtendedCrackResult result;
            master_crack(&result, &options, &dict, &shadow_entry, type, result_type);
            handle_result(&options, &result, &overview_result, result_file);
        }
    }

    CrackJob *jobs = calloc(options.world_size, sizeof(CrackJob));
    for (int i = 0; i < options.world_size; i++) {
        memset(&jobs[i], 0, sizeof(CrackJob));
        jobs[i].action = ACTION_STOP;
    }
    MPI_Scatter(jobs, 1, *type, &jobs[0], 1, *type, 0, MPI_COMM_WORLD);
    free(jobs);
    // Handle overall result
    handle_overview_result(&options, &overview_result);

    master_cleanup(shadow_file, result_file, &dict);
    return true;
}

/*
 * Initialize master stuff.
 */
static bool init_master(Options *options, Dictionary *dict, FILE **shadow_file, FILE **result_file, int argc, char **argv) {
    // Parse CLI args
    if (!parse_cli_args(options, argc, argv)) {
        master_cleanup(*shadow_file, *result_file, dict);
        return false;
    }

    int mpi_size;
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    options->world_size = mpi_size;

    // Print some useful info
    if (!options->quiet) {
        printf("Workers: %d\n", mpi_size);
        printf("Max symbols: %ld\n", options->max_length);
        printf("Symbol separator: \"%s\"\n", options->separator);
    }

    // Open shadow file
    if (!options->quiet) {
        printf("Shadow file: %s\n", options->shadow_file);
    }
    if (!open_file(shadow_file, options->shadow_file, "r")) {
        master_cleanup(*shadow_file, *result_file, dict);
        return false;
    }
    // Open output file if provided
    if (options->result_file[0] != 0) {
        if (!options->quiet) {
            printf("Output file: %s\n", options->result_file);
        }
        if (!open_file(result_file, options->result_file, "w")) {
            master_cleanup(*shadow_file, *result_file, dict);
            return false;
        }
    }
    // Read full directory
    if (!options->quiet) {
        printf("Dictionary file: %s\n", options->dict_file);
    }
    if (!read_dictionary(dict, options, options->dict_file)) {
        master_cleanup(*shadow_file, *result_file, dict);
        return false;
    }
    return true;
}

/*
 * Cleanup master stuff.
 */
static void master_cleanup(FILE *shadow_file, FILE *result_file, Dictionary *dict) {
    if (shadow_file) {
        fclose(shadow_file);
    }
    if (result_file) {
        fclose(result_file);
    }
    if (dict->elements) {
        free(dict->elements);
    }
}

/*
 * Crack a shadow password entry as master.
 */
static void master_crack(ExtendedCrackResult *result, Options *options, Dictionary *dict, ShadowEntry *entry, MPI_Datatype *type, MPI_Datatype *result_type) {
    // Initialize result
    memset(result, 0, sizeof(ExtendedCrackResult));
    strncpy(result->user, entry->user, MAX_SHADOW_USER_LENGTH);
    strncpy(result->passfield, entry->passfield, MAX_SHADOW_PASSFIELD_LENGTH);
    result->alg = entry->alg;

    // Accept only known algs
    if (entry->alg == ALG_UNKNOWN) {
        result->status = STATUS_SKIP;
        return;
    }

    // Setup vars for cracking
    ProbeConfig config = {};
    config.dict_positions = calloc(options->max_length, sizeof(size_t));
    config.symbols = calloc(options->max_length, MAX_DICT_ELEMENT_LENGTH + 1);

    CrackJob *jobs = calloc(options->world_size, sizeof(CrackJob));
    for (int i = 0; i < options->world_size; i++) {
        strncpy(jobs[i].passfield, entry->passfield, MAX_SHADOW_PASSFIELD_LENGTH);
    }
    CrackResult *results = calloc(options->world_size, sizeof(CrackResult));

    double start_time = MPI_Wtime();

    // Try probes until the status changes (when a match is found or the search space is exhausted)
    while (result->status == STATUS_PENDING) {
        bool more_probes = false;
        for (int i = 0; i < options->world_size; i++) {
            memset(&jobs[i], 0, sizeof(CrackJob));
            more_probes = get_next_probe(&config, options, dict);
            if (!more_probes) {
                jobs[i].action = ACTION_WAIT;
                break;
            }
            jobs[i].action = ACTION_WORK;
            strncpy(jobs[i].passfield, entry->passfield, MAX_PASSWORD_LENGTH);
            jobs[i].alg = entry->alg;
            jobs[i].salt_end = entry->salt_end;
            strncpy(jobs[i].probe, config.probe, MAX_PASSWORD_LENGTH);
            if (options->verbose) {
                printf("%s\n", jobs[i].probe);
            }
        }

        CrackJob job;
        CrackResult crack_result;
        MPI_Scatter(jobs, 1, *type, &job, 1, *type, 0, MPI_COMM_WORLD);
        crack_job(&crack_result, &job);
        MPI_Gather(&crack_result, 1, *result_type, results, 1, *result_type, 0, MPI_COMM_WORLD);

        for (int i = 0; i < options->world_size; i++) {
            result->attempts++;
            if (results[i].status == STATUS_SUCCESS) {
                result->status = results[i].status;
                strncpy(result->password, results[i].password, MAX_PASSWORD_LENGTH);
                break;
            }
        }
    }
    double end_time = MPI_Wtime();
    result->duration = end_time - start_time;

    free(config.dict_positions);
    free(config.symbols);
    free(jobs);
    free(results);
}

/*
 * Build the next probe. Returns false with an empty probe when the search space is exhausted.
 */
static bool get_next_probe(ProbeConfig *config, Options *options, Dictionary *dict) {
    // Check if dict is empty
    if (dict->length == 0) {
        return false;
    }

    // Find last symbol which can be replaced with the next one, if any exists
    ssize_t last_replaceable_pos = -1;
    for (size_t i = 0; i < config->size; i++) {
        if (config->dict_positions[i] < dict->length - 1) {
            last_replaceable_pos = i;
        }
    }

    // A symbol can be replaced, replace last one and reset all behind it
    if (last_replaceable_pos >= 0) {
        size_t new_dict_pos = config->dict_positions[last_replaceable_pos] + 1;
        config->dict_positions[last_replaceable_pos] = new_dict_pos;
        strncpy(config->symbols[last_replaceable_pos], dict->elements[new_dict_pos], MAX_DICT_ELEMENT_LENGTH);
        for (size_t i = last_replaceable_pos + 1; i < config->size; i++) {
            config->dict_positions[i] = 0;
            strncpy(config->symbols[i], dict->elements[0], MAX_DICT_ELEMENT_LENGTH);
        }
    }
    // No symbols can be replaced and no more symbols are allowed, return error
    else if (config->size == options->max_length) {
        config->probe[0] = 0;
        return false;
    }
    // New symbol can be added, reset all previous positions and add it
    else {
        config->size++;
        for (size_t i = 0; i < config->size; i++) {
            config->dict_positions[i] = 0;
            strncpy(config->symbols[i], dict->elements[0], MAX_DICT_ELEMENT_LENGTH);
        }
    }

    // Build probe
    config->probe[0] = 0;
    for (size_t i = 0; i < config->size; i++) {
        if (i > 0) {
            strncat(config->probe, options->separator, MAX_PASSWORD_LENGTH);
        }
        strncat(config->probe, config->symbols[i], MAX_PASSWORD_LENGTH);
    }

    return true;
}

/*
 * Handle result from trying to crack a single password.
 */
static void handle_result(Options *options, ExtendedCrackResult *result, OverviewCrackResult *overview_result, FILE *result_file) {
    // Make representations
    char const *alg_str = cryptalg_to_string(result->alg);
    char const *status_str = crack_result_status_to_string(result->status);
    double attempts_per_second = result->attempts / result->duration;

    // Format and print
    static size_t const max_output_length = 1023;
    char *output = malloc(max_output_length + 1);
    snprintf(output, max_output_length + 1, "user=\"%s\" alg=\"%s\" status=\"%s\" duration=\"%fs\" attempts=\"%ld\" attempts_per_second=\"%f\" password=\"%s\"",
             result->user, alg_str, status_str, result->duration, result->attempts, attempts_per_second, result->password);
    if (!options->quiet) {
        printf("%s\n", output);
    }
    if (result_file) {
        fprintf(result_file, "%s\n", output);
        fflush(result_file);
    }
    free(output);

    // Update overview
    overview_result->statuses[result->status]++;
    overview_result->duration += result->duration;
    overview_result->attempts += result->attempts;
}

/*
 * Handle result from trying to crack all passwords.
 */
static void handle_overview_result(Options *options, OverviewCrackResult *result) {
    if (!options->quiet) {
        printf("\nOverview:\n");
        printf("Total duration: %.3fs\n", result->duration);
        printf("Total attempts: %ld\n", result->attempts);
        printf("Total attempts per second: %.3f\n", result->attempts / result->duration);
        printf("Skipped: %ld\n", result->statuses[STATUS_SKIP]);
        printf("Successful: %ld\n", result->statuses[STATUS_SUCCESS]);
        printf("Failed: %ld\n", result->statuses[STATUS_FAIL]);
    }
}

/*
 * Hash probe and compare.
 */
static void crack_job(CrackResult *result, CrackJob *job) {
    memset(result, 0, sizeof(CrackResult));

    // Only accept known (redundant check)
    if (job->alg == ALG_UNKNOWN) {
        result->status = STATUS_SKIP;
        return;
    }

    char const *new_passfield = crypt(job->probe, job->passfield);
    if (new_passfield != NULL && strncmp(job->passfield, new_passfield, MAX_SHADOW_PASSFIELD_LENGTH) == 0) {
        // Match found, abort search
        result->status = STATUS_SUCCESS;
        strncpy(result->password, job->probe, MAX_PASSWORD_LENGTH);
        result->password[MAX_PASSWORD_LENGTH] = 0;
    }
}
